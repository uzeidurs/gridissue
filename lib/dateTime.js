/* A simple DateTime library used by this example */
var moment = require("lib/moment.js")

exports.monthDays = function(date) {
    var d = new Date(date.getFullYear(), date.getMonth()+1, 0)
    return d.getDate()
}

exports.first = function(date) {
	var d = new Date(date.valueOf())
	d.setDate(1)
	return d
}



exports.nextDay = function(date) {
	var d = new Date(date.valueOf())
	d.setDate( d.getDate() + 1)
	return d
}


exports.firstDayWeek = function(date){
	var d = new Date(date)
	var first = moment(d).startOf('isoweek')
	return new Date(first)
}





exports.dayOfWeek = function(date) {
	return (date.getDay() + 6) % 7 //shift to Mon-Sun
}

exports.monthLabels = [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre" ]

exports.dayLabels = [ "L", "M", "M", "J", "V", "S", "D" ]
